#include <stdio.h>
#include <stdlib.h>

#include "cuda_auxiliary.h"

#define BLOCK_SIZE 512

void usage(char *name) {
    fprintf(stderr, "Usage: %s input_file output_file N\n", name);
    exit(EXIT_FAILURE);
}

void read_data(char *filename, size_t N , double *h_a, double *h_b,
               double *h_c, double *h_d) {
    FILE *fin;
    open_file(fin, filename, "r");

    h_a[0] = 0;
    for (int i = 1; i < N; ++i) {
        fscanf(fin, "%lf", h_a + i);
    }

    for (int i = 0; i < N; ++i) {
        fscanf(fin, "%lf", h_b + i);
    }

    for (int i = 0; i < N - 1; ++i) {
        fscanf(fin, "%lf", h_c + i);
    }
    h_c[N-1] = 0;

    for (int i = 0; i < N; ++i) {
        fscanf(fin, "%lf", h_d + i);
    }

    close_file(fin);
}

void write_result(char *filename, size_t N, double *h_r) {
    FILE *fout;
    open_file(fout, filename, "w");
    for (int i = 0; i < N; ++i) {
        fprintf(fout, "%.16lf ", h_r[i]);
    }
    fprintf(fout, "\n");
    close_file(fout);
}

__global__ void dev_split(size_t N, double * __restrict__ d_a,
                          double * __restrict__ d_b, double * __restrict__ d_c,
                          double * __restrict__ d_d, double * __restrict__ c_a,
                          double * __restrict__ c_b, double * __restrict__ c_c,
                          double * __restrict__ c_d, size_t offset) {
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int left = i - offset;
    const int right = i + offset;
    double t_a, t_b, t_c, t_d, A, C;
    if (i >= N) {
        return;
    }

    t_a = 0;
    t_b = d_b[i];
    t_c = 0;
    t_d = d_d[i];

    if (left >= 0) {
        A = d_a[i] / d_b[left];
        t_a = -d_a[left] * A;
        t_b -= d_c[left] * A;
        t_d -= d_d[left] * A;
    }

    if (right < N) {
        C = d_c[i] / d_b[right];
        t_c = -d_c[right] * C;
        t_b -= d_a[right] * C;
        t_d -= d_d[right] * C;
    }

    c_a[i] = t_a;
    c_b[i] = t_b;
    c_c[i] = t_c;
    c_d[i] = t_d;
}

__global__ void dev_tridiag(size_t N, double * __restrict__ d_a,
                            double * __restrict__ d_b,
                            double * __restrict__ d_c,
                            double * __restrict__ d_d,
                            double * __restrict__ d_r,
                            size_t mem_offset) {
    const int i = threadIdx.x;
    const int j = i < BLOCK_SIZE / 2 ? i + BLOCK_SIZE / 2 : i - BLOCK_SIZE / 2;
    const size_t mem_pos = i * mem_offset + blockIdx.x;
    __shared__ double s_a[BLOCK_SIZE];
    __shared__ double s_b[BLOCK_SIZE];
    __shared__ double s_c[BLOCK_SIZE];
    __shared__ double s_d[BLOCK_SIZE];
    double n_a, n_b, n_c, n_d, A, C;
    int offset = 1;

    if (mem_pos < N) {
        s_a[i] = d_a[mem_pos];
        s_b[i] = d_b[mem_pos];
        s_c[i] = d_c[mem_pos];
        s_d[i] = d_d[mem_pos];
    } else {
        s_a[i] = 0;
        s_b[i] = 1;
        s_c[i] = 0;
        s_d[i] = 0;
    }

    do {
        __syncthreads();

        n_a = 0;
        n_b = s_b[i];
        n_c = 0;
        n_d = s_d[i];

        if (i - offset >= 0) {
            A = s_a[i] / s_b[i - offset];
            n_a = -s_a[i - offset] * A;
            n_b -= s_c[i - offset] * A;
            n_d -= s_d[i - offset] * A;
        }

        if (i + offset < BLOCK_SIZE) {
            C = s_c[i] / s_b[i + offset];
            n_c = -s_c[i + offset] * C;
            n_b -= s_a[i + offset] * C;
            n_d -= s_d[i + offset] * C;
        }

        __syncthreads();

        s_a[i] = n_a;
        s_b[i] = n_b;
        s_c[i] = n_c;
        s_d[i] = n_d;

        offset <<= 1;

    } while (offset < BLOCK_SIZE / 2);

    __syncthreads();

    if (mem_pos < N) {
        if (i < j) {
            d_r[mem_pos] = (s_d[i] * s_b[j] - s_c[i] * s_d[j]) /
                           (s_b[i] * s_b[j] - s_c[i] * s_a[j]);
        } else {
            d_r[mem_pos] = (s_d[i] * s_b[j] - s_a[i] * s_d[j]) /
                           (s_b[j] * s_b[i] - s_c[j] * s_a[i]);
        }
    }

}

int main(int argc, char *argv[]) {

    if (argc != 4) {
        usage(argv[0]);
    }

    double *h_a, *h_b, *h_c, *h_d;
    double *d_a[2], *d_b[2], *d_c[2], *d_d[2];
    size_t N = atoi(argv[3]);
    size_t alloc_size = N * sizeof(double);
    int step = 0, offset = 1;

    // allocate memory
    host_alloc(h_a, double, N);
    host_alloc(h_b, double, N);
    host_alloc(h_c, double, N);
    host_alloc(h_d, double, N);

    for (int i = 0; i < 2; ++i) {
        cuda_exec(cudaMalloc(d_a + i, alloc_size));
        cuda_exec(cudaMalloc(d_b + i, alloc_size));
        cuda_exec(cudaMalloc(d_c + i, alloc_size));
        cuda_exec(cudaMalloc(d_d + i, alloc_size));
    }

    read_data(argv[1], N, h_a, h_b, h_c, h_d);

    // copy data to device
    cuda_exec(cudaMemcpy(d_a[0], h_a, alloc_size, cudaMemcpyHostToDevice));
    cuda_exec(cudaMemcpy(d_b[0], h_b, alloc_size, cudaMemcpyHostToDevice));
    cuda_exec(cudaMemcpy(d_c[0], h_c, alloc_size, cudaMemcpyHostToDevice));
    cuda_exec(cudaMemcpy(d_d[0], h_d, alloc_size, cudaMemcpyHostToDevice));

    cuda_exec(cudaDeviceSetCacheConfig(cudaFuncCachePreferShared));

    // calculate result

    while ((N + offset - 1) / offset > BLOCK_SIZE) {
        dev_split<<<(N + BLOCK_SIZE - 1) / BLOCK_SIZE, BLOCK_SIZE>>>(
                N, d_a[step], d_b[step], d_c[step], d_d[step], d_a[1-step],
                d_b[1-step], d_c[1-step], d_d[1-step], offset);
        cuda_exec(cudaDeviceSynchronize());
        offset <<= 1;
        step = 1 - step;
    }

    //offset >>= 1;

    dev_tridiag<<<offset, BLOCK_SIZE>>>(
            N, d_a[step], d_b[step], d_c[step], d_d[step], d_d[1-step],
            offset);

    // copy result from device
    cuda_exec(cudaMemcpy(h_d, d_d[1-step], alloc_size,
              cudaMemcpyDeviceToHost));

    write_result(argv[2], N, h_d);

    // dealocate memory
    for (int i = 0; i < 2; ++i) {
        cuda_exec(cudaFree(d_a[i]));
        cuda_exec(cudaFree(d_b[i]));
        cuda_exec(cudaFree(d_c[i]));
        cuda_exec(cudaFree(d_d[i]));
    }

    host_free(h_a);
    host_free(h_b);
    host_free(h_c);
    host_free(h_d);

    return 0;
}

