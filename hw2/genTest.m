#!/usr/bin/octave -qf

if (nargin != 2)
    usage([program_name() " N outfile"]);
endif

n = base2dec(argv(){1}, 10);
outFileName = argv(){2};

a = rand(1, n-1);
b = rand(1, n);
c = rand(1, n-1);
d = rand(1, n);

dlmwrite(outFileName, a, "-append", "delimiter", " ");
dlmwrite(outFileName, b, "-append", "delimiter", " ");
dlmwrite(outFileName, c, "-append", "delimiter", " ");
dlmwrite(outFileName, d, "-append", "delimiter", " ");

