#!/usr/bin/octave -qf

if (nargin != 1)
    usage([program_name() " infile"]);
endif

inFileName = argv(){1};

A = dlmread(inFileName);

n = columns(A);

a = A(1, 1:n-1);
b = A(2, :);
c = A(3, 1:n-1);
d = A(4, :);

T = diag(a, -1) + diag(b) + diag(c, 1);

disp((T \ d')');
