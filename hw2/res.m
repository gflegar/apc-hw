#!/usr/bin/octave -qf

if (nargin != 2)
    usage([program_name() " infile outfile"]);
endif

inFileName = argv(){1};
outFileName = argv(){2};

A = dlmread(inFileName);

n = columns(A);

a = A(1, 1:n-1);
b = A(2, :);
c = A(3, 1:n-1);
d = A(4, :);

T = sparse(diag(a, -1)) + sparse(diag(b)) + sparse(diag(c, 1));

x = dlmread(outFileName);

disp(T * x' - d');
