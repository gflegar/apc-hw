#!/usr/bin/octave -qf

if (nargin != 2)
    usage([program_name() " N outfile"]);
endif

n = base2dec(argv(){1}, 10);
outFileName = argv(){2};

x = rand(n, 1);
disp(x(1:min(10, n)));

outFile = fopen(outFileName, "wb");
fwrite(outFile, x, "double");
fclose(outFile);

