#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "cuda_auxiliary.h"

#define SCAL_BLOCK_SIZE 1024
#define MULT_BLOCK_SIZE 32

int verbose = 0;
int timed = 0;

void printUsage(char *argv[]) {
    fprintf(stderr, "Usage: %s N A.dat x0.dat eps [TIME] [VERBOSE]\n",
            argv[0]);
    exit(EXIT_FAILURE);
}

void writeMatrix(FILE *fout, int N, int M, double *A, int lda) {
    if (N > 10) {
        N = 10;
    }
    if (M > 10) {
        M = 10;
    }
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            fprintf(fout, "%10.6e ", A[i * lda + j]);
        }
        fprintf(fout, "\n");
    }
}

void writeMatrixFromDevice(FILE *fout, int N, int M, double *d_A, int lda) {
    double *h_A;
    host_alloc(h_A, double, N * M);
    cuda_exec(cudaMemcpy2D(h_A, M * sizeof(double), d_A, lda * sizeof(double),
                           M * sizeof(double), N, cudaMemcpyDeviceToHost));
    writeMatrix(fout, N, M, h_A, M);
    host_free(h_A);
}


void readData(int N, double *d_A, size_t pitch, double *d_x, double *d_y,
              const char *filenameA, const char *filenameX0) {
    double *h_A;
    double *h_x0;
    FILE *ain;
    FILE *x0in;

    cuda_exec(cudaMallocHost(&h_A, N * N * sizeof(double)));
    cuda_exec(cudaMallocHost(&h_x0, N * sizeof(double)));

    open_file(ain, filenameA, "rb");
    read_file(h_A, sizeof(double), N * N, ain);
    cuda_exec(cudaMemcpy2DAsync(d_A, pitch, h_A, N * sizeof(double),
                                N * sizeof(double), N,
                                cudaMemcpyHostToDevice, 0));
    close_file(ain);

    open_file(x0in, filenameX0, "rb");
    read_file(h_x0, sizeof(double), N, x0in);
    cuda_exec(cudaMemcpyAsync(d_y, h_x0, N * sizeof(double),
                              cudaMemcpyHostToDevice, 0));
    close_file(x0in);

    if (verbose) {
        printf("A:\n");
        writeMatrix(stdout, N, N, h_A, N);
        printf("x0:\n");
        writeMatrix(stdout, N, 1, h_x0, 1);
    }

    cuda_exec(cudaStreamSynchronize(0));
    cuda_exec(cudaFreeHost(h_A));
    cuda_exec(cudaFreeHost(h_x0));
}

__global__ void dev_calcNrmsStart(int N, double * __restrict__ d_x,
                                  double * __restrict__ d_y,
                                  double lambda,
                                  double * __restrict__ d_work,
                                  size_t ldw) {
    __shared__ double smem[2 * SCAL_BLOCK_SIZE];
    const int ind = blockIdx.x * blockDim.x + threadIdx.x;
    const int indy = threadIdx.x;
    const int indx = threadIdx.x + SCAL_BLOCK_SIZE;
    double thx = 0.0;
    double thy = 0.0;
    if (ind < N) {
        thy = d_y[ind];
        thx = thy - lambda * d_x[ind];
    }
    smem[indy] = thy * thy;
    smem[indx] = thx * thx;
    for (int memSize = SCAL_BLOCK_SIZE >> 1; memSize; memSize >>= 1) {
        __syncthreads();
        if (threadIdx.x < memSize) {
            smem[indy] += smem[indy + memSize];
            smem[indx] += smem[indx + memSize];
        }
    }
    if (threadIdx.x == 0) {
        d_work[blockIdx.x] = smem[0];
        d_work[blockIdx.x + ldw] = smem[SCAL_BLOCK_SIZE];
    }
}

__global__ void dev_reduce(int N, double *d_work) {
    __shared__ double smem[SCAL_BLOCK_SIZE];
    const int ind = 2 * blockIdx.x * blockDim.x + threadIdx.x;
    if (ind < N) {
        smem[threadIdx.x] = d_work[ind];
    } else {
        smem[threadIdx.x] = 0.0;
    }
    if (ind + SCAL_BLOCK_SIZE < N) {
        smem[threadIdx.x] += d_work[ind + SCAL_BLOCK_SIZE];
    }
    for (int memSize = SCAL_BLOCK_SIZE >> 1; memSize; memSize >>= 1) {
        __syncthreads();
        if (threadIdx.x < memSize) {
            smem[threadIdx.x] += smem[threadIdx.x + memSize];
        }
    }
    if (threadIdx.x == 0) {
        d_work[blockIdx.x] = smem[0];
    }
}

void calcNrms(int N, double *d_x, double *d_y, double *d_work, size_t ldw,
              double lambda, double *ynrm, double *tolnrm) {
    int blockSize = SCAL_BLOCK_SIZE;
    int gridSize = (N + blockSize - 1) / blockSize;
    dev_calcNrmsStart<<<gridSize, blockSize>>>(N, d_x, d_y, lambda, d_work,
                                               ldw);
    while (gridSize > 1) {
        N = gridSize;
        gridSize = (N + 2 * blockSize - 1) / (2 * blockSize);
        dev_reduce<<<gridSize, blockSize>>>(N, d_work);
        dev_reduce<<<gridSize, blockSize>>>(N, d_work + ldw);
    }
    cuda_exec(cudaMemcpy(ynrm, d_work, sizeof(double),
                         cudaMemcpyDeviceToHost));
    cuda_exec(cudaMemcpy(tolnrm, d_work + ldw, sizeof(double),
                         cudaMemcpyDeviceToHost));
    *ynrm = sqrt(*ynrm);
    *tolnrm = sqrt(*tolnrm);
}

__global__ void dev_scaleVec(double scale, double *d_x, int N) {
    const int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind < N) {
        d_x[ind] *= scale;
    }
}

void scaleVec(double scale, double *d_x, int N) {
    const int blockSize = SCAL_BLOCK_SIZE;
    const int gridSize = (N + blockSize - 1) / blockSize;
    dev_scaleVec<<<gridSize, blockSize>>>(scale, d_x, N);
}

__global__ void dev_matMult(double *d_A, size_t lda, double *d_x,
                            double *d_y, int N) {
    __shared__ double s_A[MULT_BLOCK_SIZE][MULT_BLOCK_SIZE + 1];
    const int blockInd = blockIdx.x * blockDim.x;
    int last = min(blockInd + MULT_BLOCK_SIZE, N) - blockInd;
    double res = 0.0;
    for (int i = 0; i < N; i += MULT_BLOCK_SIZE) {
        int col = i + threadIdx.x;
        for (int j = 0; j < last; ++j) {
            if (col < N) {
                s_A[j][threadIdx.x] = d_A[(blockInd + j) * lda + col];
            }
        }
        if (col < N) {
            s_A[threadIdx.x][MULT_BLOCK_SIZE] = d_x[col];
        }
        if (threadIdx.x < last) {
            int elems = min(i + MULT_BLOCK_SIZE, N) - i;
            for (int j = 0; j < elems; ++j) {
                res += s_A[threadIdx.x][j] * s_A[j][MULT_BLOCK_SIZE];
            }
        }
    }
    d_y[blockInd + threadIdx.x] = res;
}

void matMult(double *d_A, size_t lda, double *d_x, double *d_y, int N) {
    const int blockSize = MULT_BLOCK_SIZE;
    const int gridSize = (N + blockSize - 1) / blockSize;
    dev_matMult<<<gridSize, blockSize>>>(d_A, lda, d_x, d_y, N);
}

__global__ void dev_calcDotStart(int N, double * __restrict__ d_x,
                                 double * __restrict__ d_y,
                                 double * __restrict__ d_work) {
    __shared__ double smem[SCAL_BLOCK_SIZE];
    const int ind = blockIdx.x * blockDim.x + threadIdx.x;
    if (ind < N) {
        smem[threadIdx.x] = d_x[ind] * d_y[ind];
    } else {
        smem[threadIdx.x] = 0;
    }
    for (int memSize = SCAL_BLOCK_SIZE >> 1; memSize; memSize >>= 1) {
        __syncthreads();
        if (threadIdx.x < memSize) {
            smem[threadIdx.x] += smem[threadIdx.x + memSize];
        }
    }
    if (threadIdx.x == 0) {
        d_work[blockIdx.x] = smem[0];
    }
}


void dotProd(double *d_x, double *d_y, int N, double *lambda, double *d_work) {
    int blockSize = SCAL_BLOCK_SIZE;
    int gridSize = (N + blockSize - 1) / blockSize;
    dev_calcDotStart<<<gridSize, blockSize>>>(N, d_x, d_y, d_work);
    while (gridSize > 1) {
        N = gridSize;
        gridSize = (N + 2 * blockSize - 1) / (2 * blockSize);
        dev_reduce<<<gridSize, blockSize>>>(N, d_work);
    }
    cuda_exec(cudaMemcpy(lambda, d_work, sizeof(double),
                         cudaMemcpyDeviceToHost));
}

double calcSpec(int N, double eps, double *d_A, size_t lda, double *d_x,
                double *d_y, double *d_work, size_t ldw) {
    const int blockSize = SCAL_BLOCK_SIZE;
    const int gridSize = (N + blockSize - 1) / blockSize;
    const int multBlockSize = MULT_BLOCK_SIZE;
    const int multGridSize = (N + multBlockSize - 1) / multBlockSize;
    double ynrm;
    double tolnrm;
    double lambda = 0;
    double *tmp;
    calcNrms(N, d_x, d_y, d_work, ldw, lambda, &ynrm, &tolnrm);
    do {
        if (ynrm != 0.0) {
            dev_scaleVec<<<gridSize, blockSize>>>(1.0 / ynrm, d_y, N);
        }
        tmp = d_y;
        d_y = d_x;
        d_x = tmp;
        dev_matMult<<<multGridSize, multBlockSize>>>(d_A, lda, d_x, d_y, N);
        dotProd(d_x, d_y, N, &lambda, d_work);
        calcNrms(N, d_x, d_y, d_work, ldw, lambda, &ynrm, &tolnrm);
        lambda = fabs(lambda);
    } while (tolnrm > eps * lambda);
    return lambda;
}

int main(int argc, char *argv[]) {
    int N;
    double eps;
    double specrad;
    double *d_A;
    double *d_x;
    double *d_y;
    double *d_work;
    double algoTime = 0;
    size_t pitch;
    size_t lda;
    size_t ldw;
    if (argc < 5) {
        printUsage(argv);
    }
    for (int i = 5; i < argc; ++i) {
        if (strcmp(argv[i], "VERBOSE") == 0) {
            verbose = 1;
        } else if (strcmp(argv[i], "TIME") == 0) {
            timed = 1;
        } else {
            printUsage(argv);
        }
    }

    N = atoi(argv[1]);
    eps = atof(argv[4]);

    cuda_exec(cudaMallocPitch(&d_A, &pitch, N * sizeof(double), N));
    lda = pitch / sizeof(double);
    cuda_exec(cudaMallocPitch(
                &d_work, &ldw,
                (N + SCAL_BLOCK_SIZE - 1) / SCAL_BLOCK_SIZE * sizeof(double),
                2));
    ldw /= sizeof(double);
    cuda_exec(cudaMalloc(&d_x, N * sizeof(double)));
    cuda_exec(cudaMalloc(&d_y, N * sizeof(double)));

    readData(N, d_A, pitch, d_x, d_y, argv[2], argv[3]);

    algoTime -= timer();
    cuda_exec(cudaDeviceSetCacheConfig(cudaFuncCachePreferShared));
    specrad = calcSpec(N, eps, d_A, lda, d_x, d_y, d_work, ldw);
    algoTime += timer();
    if (timed) {
        printf("Algorithm run time: %.8fs\n", algoTime);
    }
    if (verbose) {
        printf("Spectral radius: ");
    }
    printf("%20.16le\n", specrad);

    cuda_exec(cudaFree(d_A));
    cuda_exec(cudaFree(d_y));
    cuda_exec(cudaFree(d_x));
    return 0;
}

