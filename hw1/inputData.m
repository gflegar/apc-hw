#!/usr/bin/octave -qf

if (nargin != 3)
    usage([program_name() " N A.out x0.out"]);
endif

n = base2dec(argv(){1}, 10);
outFileNameA = argv(){2};
outFileNameX = argv(){3};

printf("Input A:\n");
A = scanf("%f", [n, n]);

outFile = fopen(outFileNameA, "wb");
fwrite(outFile, A, "double");
fclose(outFile);

printf("Input x0:\n");
x0 = scanf("%f", [n, 1]);

outFile = fopen(outFileNameX, "wb");
fwrite(outFile, x0, "double");
fclose(outFile);

