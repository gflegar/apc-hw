#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cublas_v2.h>

#include "cuda_auxiliary.h"

double algoTime;

void printUsage(char *argv[]) {
    fprintf(stderr, "Usage: %s N A.dat x0.dat num_step "
            "[TRANSPOSE] [FASTPOW] [TIME] [VERBOSE]\n", argv[0]);
    exit(EXIT_FAILURE);
}

void readData(int N, char *filenameA, char *filenameX0, double *A,
               double *x0, int transpose) {
    FILE *ain;
    FILE *x0in;

    open_file(ain, filenameA, "rb");
    read_file(A, sizeof(double), N * N, ain);
    close_file(ain);

    open_file(x0in, filenameX0, "rb");
    read_file(x0, sizeof(double), N, x0in);
    close_file(x0in);

    if (transpose == 0) {
        double tmp;
        for (int i = 0; i < N; ++i) {
            for (int j = i + 1; j < N; ++j) {
                tmp = A[i * N + j];
                A[i * N + j] = A[j * N + i];
                A[j * N + i] = tmp;
            }
        }
    }
}

void writeMatrix(FILE *fout, int N, int M, double *A, int lda) {
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            fprintf(fout, "%10.6e ", A[i * lda + j]);
        }
        fprintf(fout, "\n");
    }
}

void writeMatrixFromDevice(FILE *fout, int N, int M, double *d_A, int lda) {
    double *h_A;
    host_alloc(h_A, double, N * M);
    cuda_exec(cudaMemcpy2D(h_A, M * sizeof(double), d_A, lda * sizeof(double),
                           M * sizeof(double), N, cudaMemcpyDeviceToHost));
    writeMatrix(fout, N, M, h_A, M);
    host_free(h_A);
}

void calcMatPowVec(size_t N, double *d_A, size_t lda, int e, double *d_x,
                   double *d_result, cublasHandle_t handle) {
    double alpha = 1.0;
    double beta = 0.0;
    double scale;
    double *d_M;
    double *d_R;
    double *d_S;
    double *tmp;
    size_t ldm;
    size_t ldr;
    size_t lds;
    size_t tmp2;
    int setR = 0;

    algoTime += timer();

    cuda_exec(cudaMallocPitch(&d_M, &ldm, N * sizeof(double), N));
    cuda_exec(cudaMemset(d_M, 0, ldm * N));
    ldm /= sizeof(double);
    cuda_exec(cudaMallocPitch(&d_R, &ldr, N * sizeof(double), N));
    cuda_exec(cudaMemset(d_R, 0, ldr * N));
    ldr /= sizeof(double);
    cuda_exec(cudaMallocPitch(&d_S, &lds, N * sizeof(double), N));
    cuda_exec(cudaMemset(d_S, 0, lds * N));
    lds /= sizeof(double);

    algoTime -= timer();

    if (1&e) {
        cuda_exec(cudaMemcpy2D(d_R, ldr * sizeof(double), d_A,
                               lda * sizeof(double), N * sizeof(double),
                               N, cudaMemcpyDeviceToDevice));
        setR = 1;
    }
    cublas_exec(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N,
                            &alpha, d_A, lda, d_A, lda, &beta, d_M, ldm));
    for (size_t i = 2; i <= e; i <<= 1) {
        if (i&e) {
            if (setR) {
                cublas_exec(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N,
                                        N, N, &alpha, d_R, ldr, d_M, ldm,
                                        &beta, d_S, lds));
                tmp = d_S;
                d_S = d_R;
                d_R = tmp;
                tmp2 = lds;
                lds = ldr;
                ldr = tmp2;
                cublas_exec(cublasDasum(handle, N * ldr, d_R, 1, &scale));
                if (scale != 0.0) {
                    scale = 1.0 / scale;
                    cublas_exec(cublasDscal(handle, N * ldr, &scale, d_R, 1));
                }
            } else {
                setR = 1;
                cuda_exec(cudaMemcpy2D(
                            d_R, ldr * sizeof(double), d_M,
                            ldm * sizeof(double), N * sizeof(double), N,
                            cudaMemcpyDeviceToDevice));
            }
        }
        cublas_exec(cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, N, N, N,
                                &alpha, d_M, ldm, d_M, ldm, &beta, d_S, lds));
        tmp = d_S;
        d_S = d_M;
        d_M = tmp;
        tmp2 = lds;
        lds = ldm;
        ldm = tmp2;
        cublas_exec(cublasDasum(handle, N * ldm, d_M, 1, &scale));
        if (scale != 0.0) {
            scale = 1.0 / scale;
            cublas_exec(cublasDscal(handle, N * ldm, &scale, d_M, 1));
        }
    }
    cublas_exec(cublasDgemv(handle, CUBLAS_OP_N, N, N, &alpha, d_R, ldr, d_x,
                            1, &beta, d_result, 1));
}

double calcSpec(size_t N, double *d_A, size_t lda, double *d_x, double *d_y,
                int iters, cublasHandle_t handle) {
    double norm;
    double lambda;
    double alpha = 1.0;
    double beta = 0.0;
    double *tmp;
    for (int i = 0; i < iters; ++i) {
        cublas_exec(cublasDnrm2(handle, N, d_y, 1, &norm));
        if (norm != 0.0) {
            norm = 1.0 / norm;
            cublas_exec(cublasDscal(handle, N, &norm, d_y, 1));
        }
        tmp = d_y;
        d_y = d_x;
        d_x = tmp;
        cublas_exec(cublasDgemv(handle, CUBLAS_OP_N, N, N, &alpha, d_A, lda,
                                d_x, 1, &beta, d_y, 1));
    }
    cublas_exec(cublasDdot(handle, N, d_x, 1, d_y, 1, &lambda));
    return fabs(lambda);
}

int main(int argc, char *argv[]) {
    int transpose = 0;
    int fastPow = 0;
    int verbose = 0;
    int showTime = 0;
    size_t N;
    int iters;
    size_t pitch;
    size_t lda;
    double *h_A = NULL;
    double *h_x0 = NULL;
    double *d_A = NULL;
    double *d_x = NULL;
    double *d_y = NULL;
    double specrad;
    cublasHandle_t handle;

    if (argc < 5) {
        printUsage(argv);
    }

    for (int i = 5; i < argc; ++i) {
        if (strcmp(argv[i], "TRANSPOSE") == 0) {
            transpose = 1;
        } else if (strcmp(argv[i], "FASTPOW") == 0) {
            fastPow = 1;
        } else if (strcmp(argv[i], "VERBOSE") == 0) {
            verbose = 1;
        } else if (strcmp(argv[i], "TIME") == 0) {
            showTime = 1;
        } else {
            printUsage(argv);
        }
    }

    N = atoi(argv[1]);
    iters = atoi(argv[4]);
    host_alloc(h_A, double, N * N);
    host_alloc(h_x0, double, N);

    readData(N, argv[2], argv[3], h_A, h_x0, transpose);

    if (verbose) {
        printf("A:\n");
        writeMatrix(stdout, N, N, h_A, N);
        printf("X:\n");
        writeMatrix(stdout, N, 1, h_x0, 1);
    }


    cuda_exec(cudaMallocPitch(&d_A, &pitch, N * sizeof(double), N));
    cuda_exec(cudaMemset(d_A, 0, pitch * N));
    lda = pitch / sizeof(double);
    cuda_exec(cudaMalloc(&d_x, N * sizeof(double)));
    cuda_exec(cudaMalloc(&d_y, N * sizeof(double)));
    cublas_exec(cublasSetMatrix(N, N, sizeof(double), h_A, N, d_A, lda));
    cublas_exec(cublasSetVector(N, sizeof(double), h_x0, 1, d_y, 1));

    cublas_exec(cublasCreate(&handle));

    algoTime = -timer();

    if (fastPow && iters > 1) {
        double *tmp;
        calcMatPowVec(N, d_A, lda, iters - 1, d_y, d_x, handle);
        tmp = d_x;
        d_x = d_y;
        d_y = tmp;
        iters = 1;
    }
    specrad = calcSpec(N, d_A, lda, d_x, d_y, iters, handle);
    algoTime += timer();

    if (showTime) {
        printf("Algorithm run time: %.8fs\n", algoTime);
    }
    if (verbose) {
        printf("Spectral radius: ");
    }
    printf("%20.16le\n", specrad);

    cublas_exec(cublasDestroy(handle));

    cuda_exec(cudaFree(d_A));
    cuda_exec(cudaFree(d_x));
    cuda_exec(cudaFree(d_y));

    host_free(h_A);
    host_free(h_x0);

    return 0;
}

