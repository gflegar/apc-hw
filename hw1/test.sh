#!/bin/bash

#test config
iters=20
eps=1e-15
withTime=

#compile
echo -n "compiling... "
nvcc specstp.cu -o specstp -arch=sm_20 -lcublas
nvcc speceps.cu -o speceps -arch=sm_20
echo DONE
#create output directory
mkdir -p out

echo Runing tests:
#test files
for testNumber in $@; do
    echo Runing test $testNumber:
    inFileA=dat/A$testNumber.dat
    inFileX=dat/x$testNumber.dat
    outFile=out/test$testNumber.out
    rm out/test$testNumber.out
    touch out/test$testNumber.out
    echo "test$testNumber:" >> $outFile
    echo spec.m
    echo -ne "spec.m\t\t:  " >> $outFile
    ./spec.m $testNumber $inFileA >> $outFile
    echo specstp
    echo -ne "specstp\t\t:  " >> $outFile
    ./specstp $testNumber $inFileA $inFileX $iters $withTime >> $outFile
    echo "specstp (FASTPOW)"
    echo -ne "specstp (FP)\t:  " >> $outFile
    ./specstp $testNumber $inFileA $inFileX $iters $withTime FASTPOW >> $outFile
    echo speceps
    echo -ne "speceps\t\t:  " >> $outFile
    ./speceps $testNumber $inFileA $inFileX $eps $withtime >> $outFile
done
