#!/usr/bin/octave -qf

if (nargin != 2)
    usage([program_name() " N infile"]);
endif

n = base2dec(argv(){1}, 10);
inFileName = argv(){2};

inFile = fopen(inFileName, "rb");
A = fread(inFile, [n, n], "double");
fclose(inFile);

printf("%20.16e\n", eigs(A', 1));

