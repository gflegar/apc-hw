#!/usr/bin/octave -qf

if (nargin != 2)
    usage([program_name() " N outfile"]);
endif

n = base2dec(argv(){1}, 10);
outFileName = argv(){2};

A = rand(n, n);
disp(A(1:min(10, n), 1:min(10, n)));

outFile = fopen(outFileName, "wb");
fwrite(outFile, A, "double");
fclose(outFile);

