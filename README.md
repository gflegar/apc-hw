apc-hw
======

This repository contains solutions of homeworks (using CUDA) for the course
"Application of parallel computers" ("Primjena paralelnih računala")  and final
project for the course "Intro to parallel computing" ("Uvod u paralelno
racunanje") held at University of Zagreb, Department of Mathematics
(academic year 2014/2015).

Directory structure
-------------------
* `ipc_final` -- MPI implementation of Poisson equation solver
* `hw1` -- CUDA implementation of power iteration method for the eigenvalue
           problem
* `hw2` -- CUDA implementation of PCR method for tridiagonal linear systems

