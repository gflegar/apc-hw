function [u, err, xx, yy] = test(filename, ni, nj, g, f)
  h = 1 / (ni + 1);
  fc = @(x, y) arrayfun(f, x, y / h / (nj + 1));
  gc = @(x, y) arrayfun(g, x, y / h / (nj + 1));
  tmp = reshape(textread(filename), ni, nj);
  u = [gc(zeros(1, nj + 2), 0:h:h*(nj+1)); ...
        gc((h:h:h*ni)', zeros(ni, 1)) ...
        tmp ...
        gc((h:h:h*ni)', h * (nj + 1) * ones(ni, 1)); ...
       gc(ones(1, nj + 2), 0:h:h*(nj+1))];
  [yy, xx] = meshgrid((0:h:h*(nj+1))',(0:h:1)');
  err = (u - fc(xx, yy)) ./ fc(xx, yy);
endfunction