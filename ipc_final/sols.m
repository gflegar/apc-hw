g1 = @(x, y) 0;
f1 = @(x, y) - sin(pi * x) * sin(pi * y) / (2 * pi^2);

g2 = @(x, y) y;
f2 = @(x, y) - sin(pi * x) * sin(pi * y) + y;

g3 = @(x, y) exp(x^2 + y^2);
f3 = @(x, y) exp(x^2 + y^2);

g4 = @(x, y) (x - 0.5)^3 * cos(y - 0.5);
f4 = @(x, y) (x - 0.5)^3 * cos(y - 0.5);