#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <mpi/mpi.h>

#define M_PI 3.14159265358979323846

typedef double (*Function)(double, double);

#ifdef _T1
#define _TEST
double f(double x, double y) { return sin(M_PI * x) * sin(M_PI * y); }
double g(double x, double y) { return 0; }
#endif


#ifdef _T2
#define _TEST
double f(double x, double y) { return 2 * M_PI * M_PI * sin(M_PI * x) * sin(M_PI * y); }
double g(double x, double y) { return y; }
#endif


#ifdef _T3
#define _TEST
double f(double x, double y) { return 4 * (x * x + y * y + 1) * exp(x * x + y * y); }
double g(double x, double y) { return exp(x * x + y * y); }
#endif

#ifdef _T4
#define _TEST
double f(double x, double y) {return (x - 0.5) * (6 - (x - 0.5) * (x - 0.5)) * cos(y - 0.5);}
double g(double x, double y) {return (x - 0.5) * (x - 0.5) * (x - 0.5) * cos(y - 0.5);}
#endif

#ifndef _TEST
double f(double x, double y) { return sin(M_PI * x) * sin(M_PI * y);}
double g(double x, double y) { return y;}
#endif

/**
 * Vraca maksimum brojeva `a` i `b`.
 */
inline long long max(long long a, long long b) {
    return (a > b) ? a : b;
}
/**
 * Vraca minimum brojeva `a` i `b`.
 */
inline long long min(long long a, long long b) {
    return (a < b) ? a : b;
}
/**
 * Vraca indeks prvog elementa vektora koji obradjuje proces s rangom `r`.
 * @param n Duljina vektora
 * @param r Rang procesa
 * @param s Broj procesa
 */
inline long long getStartPosition(long long n, long long r, long long s) {
    const long long ns = n / s, o = n - ns * s;
    return r * ns + (r * o) / s;
}
/**
 * Pronalazi proces koji obradjuje element s ineksom `ind` koristeci binarno pretrazivanje.
 * @param ind Indeks elementa
 * @param n Duljina vektora
 * @param procNum Broj procesa
 */
int findProcessInCharge(long long ind, long long n, int procNum) {
    int lo = 0, hi = procNum, mid;
    long long pos;
    while (lo + 1 < hi) {
        mid = lo + (hi - lo) / 2;
        pos = getStartPosition(n, mid, procNum);
        if (pos <= ind) lo = mid;
        else hi = mid;
    }
    return lo;
}
/**
 * U `a` sprema linearnu kombinaciju `alpha * a + beta * b`.
 * @param length Duljina vektora `a` i `b`
 */
void linearComb(double alpha, double *a, double beta, double *b, long long length) {
    for (int i = 0; i < length; ++i) {
        a[i] = alpha * a[i] + beta * b[i];
    }
}
/**
 * Vraca skalarni produkt vektora `a` i `b`.
 * @param length Duljina vektora
 */
double dotProd(double *a, double *b, long long length) {
    double res = 0.0;
    for (int i = 0; i < length; ++i) {
        res += a[i] * b[i];
    }
    return res;
}
/**
 * Generira dio desne strane diskretizirane Poissonove jednadzbe.
 * @param b Vektor u koji ce se spremiti desna strana
 * @param length Duljina vektora `b`
 * @param ni broj tocaka diskretizacije po prvoj koordinati
 * @param nj broj tocaka diskretizacije po drugoj koordinati
 * @param start Prvi indeks dijela koji ce se spremiti u vektor
 * @param F desna strana Poissonove jednadzbe na [0,1] x [0,1]
 * @param G funkcija Dirichleteovih rubnih uvjeta na [0,1] x [0,1]
 */
void generateRightSide(double *b, long long length, long long ni, long long nj, long long start,
                       Function F, Function G) {
    double h = 1.0 / (ni + 1.0), a = 1 / (nj + 1.0) / h;
    for (int i = 0; i < length; ++i) {
        double x = (((start + i) % ni) + 1) * h;
        double y = a * (((start + i) / ni) + 1) * h;
        b[i] = -F(x, y) * h * h;
        if ((start + i) % ni == 0) {
            b[i] += G(0.0, y);
        }
        if ((start + i + 1) % ni == 0) {
            b[i] += G(1.0, y);
        }
        if (start + i < ni) {
            b[i] += G(x, 0.0);
        }
        if (start + i >= ni * (nj - 1)) {
            b[i] += G(x, 1.0);
        }
    }
}
/**
 * Generira `comRanks` i `comRanges` potrebne funkciji `matrixMult()`.
 * @param ni Broj tocaka diskretizacije po prvoj koordinati
 * @param nj Broj tocaka diskretizacije po drugoj koordinati
 * @param start Pocetni indeks bloka koji obradjuje proces
 * @param end Indeks nakon zadnjeg indeksa kojeg obradjuje proces
 * @param rank Rang procesa
 * @param procNum Broj procesa
 */
void getMatrixMultInfo(long long ni, long long nj, long long start, long long end, int rank,
                       int procNum, int comRanks[6], long long comRanges[4]) {
    long long length = end - start;
    if (length > 0) {
        comRanges[0] = max(ni - start, 0);
        if (comRanges[0] >= length) {
            comRanks[0] = comRanks[1] = -1;
        } else {
            comRanks[0] = findProcessInCharge(comRanges[0] + start - ni, ni * nj, procNum);
            comRanges[1] = min(getStartPosition(ni * nj, comRanks[0] + 1, procNum) - start + ni,
                    length);
            if (comRanges[1] < length) {
                comRanks[1] = findProcessInCharge(comRanges[1] + start - ni, ni * nj, procNum);
            } else {
                comRanks[1] = -1;
            }
        }
        if (start % ni) {
            comRanks[2] = findProcessInCharge(start - 1, ni * nj, procNum);
        } else {
            comRanks[2] = -1;
        }
        if (end % ni) {
            comRanks[3] = findProcessInCharge(end, ni * nj, procNum);
        } else {
            comRanks[3] = -1;
        }

        if (start + ni >= ni * nj) {
            comRanks[4] = comRanks[5] = -1;
        } else {
            comRanks[4] = findProcessInCharge(start + ni, ni * nj, procNum);
            comRanges[2] = min(getStartPosition(ni * nj, comRanks[4] + 1, procNum) - start - ni,
                    length);
            if (comRanges[2] < length && start + comRanges[2] + ni < ni * nj) {
                comRanks[5] = findProcessInCharge(start + comRanges[2] + ni, ni * nj, procNum);
                comRanges[3] = min(getStartPosition(ni * nj, comRanks[5] + 1, procNum), length);
            } else {
                comRanks[5] = -1;
            }
        }

        for (int i = 0; i < 6; ++i) {
            if (comRanks[i] == rank) {
                comRanks[i] = -2;
            }
        }
    } else  {
        for (int i = 0; i < 6; ++i) {
            comRanks[i] = -1;
        }
    }
}
/**
 * Mnozi vektor `d` dijelom matrice dobive diskretizacijom Poissonove jednadzbe i sprema rezultat
 * u `ad`.
 * @param w1 Pomocni vektor duljine `length`
 * @param w2 Pomocni vektor duljine `length`
 * @param length Duljina vektora `d` i `ad`
 * @param start Prvi indeks vektora kojeg proces obradjuje
 * @param ni Broj tocaka diskretizacije po prvoj koordinati
 * @param comRanks Informacija dobivena funkcijom `getMatrixMultInfo()`
 * @param comRanges Informacija dobivena funkcijom `getMatrixMultInfo()`
 */
void matrixMult(const double *d, double *ad, double *w1, double *w2, long long length,
                long long start, long long ni, const int comRanks[6],
                const long long comRanges[4]) {
    MPI_Request sendRequest[6], recvRequest[6];
    int doneRequests[6];
    int doneLength, comsLeft = 6;
    double wb, wa;
    const long long ranges[6][2] = {{comRanges[0], comRanges[1]}, {comRanges[1], length}, {0, 1},
                                    {length - 1, length}, {0, comRanges[2]},
                                    {comRanges[2], comRanges[3]}};
    const long long ownedOffsets[6] = {-ni, -ni, 0, 0, ni, ni};
    double *mems[6] = {w1, w1, &wb, &wa - length + 1, w2, w2};

    // zapocni razmjenu podataka
    for (int c = 0; c < 6; ++c) {
        if (comRanks[c] < 0) {
            --comsLeft;
            sendRequest[c] = recvRequest[c] = MPI_REQUEST_NULL;
        } else {
            MPI_Isend(d + ranges[c][0], ranges[c][1] - ranges[c][0], MPI_DOUBLE,
                    comRanks[c], (c == 2 || c == 3) ? 1 : 0, MPI_COMM_WORLD, sendRequest + c);
            MPI_Irecv(mems[c] + ranges[c][0], ranges[c][1] - ranges[c][0], MPI_DOUBLE,
                    comRanks[c], (c == 2 || c == 3) ? 1 : 0, MPI_COMM_WORLD, recvRequest + c);
        }
    }

    // izracunaj ono sto mozes sa svojim podacima
    for (int i = 0; i < length; ++i) {
        ad[i] = 4.0 * d[i];
        if (i > 0 && (start + i) % ni) {
            ad[i] -= d[i - 1];
        }
        if (i + 1 < length && (start + i + 1) % ni) {
            ad[i] -= d[i + 1];
        }
    }
    for (int c = 0; c < 6; ++c) {
        if (comRanks[c] == -2) {
            for (int i = ranges[c][0]; i < ranges[c][1]; ++i) {
                ad[i] -= d[i + ownedOffsets[c]];
            }
        }
    }

    // izracunaj ostatak kad dobijes podatke
    while (comsLeft) {
        MPI_Waitsome(6, recvRequest, &doneLength, doneRequests, MPI_STATUSES_IGNORE);
        comsLeft -= doneLength;
        for (int j = 0; j < doneLength; ++j) {
            int c = doneRequests[j];
            for (int i = ranges[c][0]; i < ranges[c][1]; ++i) {
                ad[i] -= mems[c][i];
            }
        }
    }

    // pricekaj da se zavrsi slanje podataka
    MPI_Waitall(6, sendRequest, MPI_STATUSES_IGNORE);
}

int main(int argc, char *argv[]) {
    // podaci o problemu
    const double eps = 1.0e-14; // tocnost
    Function F = f, G = g; // funkcija i rubni uvjeti
    long long ni, nj; // broj tocaka diskretizacije

    // podaci o procesu
    int rank, procNum; // broj procesa, ukupan broj procesa
    long long start, end, length; //pocetak, kraj i velicina bloka kojeg racuna proces
    int comRanks[6]; // procesi s kojima treba komunikacija
    long long comRanges[4]; // informacija o podacima ostalih procesa
    MPI_Request reduceRequest; // request za neblokirajucu redukciju

    // podaci za ispis u datoteku
    char *filename; // ime datoteke
    MPI_File fout; // pointer na datoteku
    char buff[20]; // buffer za konverziju broja u znakove
    int outSize; // duljina broja konvertiranog u znakove
    MPI_Aint charExtent; // velicina char-a u datoteci

    // memorija potrebna za program
    double *x, *r, *d, *ad, *w1, *w2; // vektori rjesenja, reziduala, smjera pretrazivana, pomocni
    double **mem[6] = {&x, &r, &d, &ad, &w1, &w2}; // niz gornjih vektora za laksu alokaciju
    double oldRR, newRR, tau, dAd; // varijable za spremanje skalarnih produkata

    // inicijalizacija MPI-a
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procNum);

    // racunanje informacija o problemu
    if (argc != 4) {
        if (rank == 0) {
            fprintf(stderr, "Usage: %s NI NJ out_file\n", argv[0]);
        }
        MPI_Finalize();
        return 0;
    }
    ni = atoll(argv[1]);
    nj = atoll(argv[2]);
    filename = argv[3];

    // racunanje podataka o trenutnom procesu
    start = getStartPosition(ni * nj, rank, procNum);
    end = getStartPosition(ni * nj, rank + 1, procNum);
    length = end - start;

    getMatrixMultInfo(ni, nj, start, end, rank, procNum, comRanks, comRanges);

#ifdef _DEBUG
    for (int i = 0; i < procNum; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        fprintf(stderr, "Process %d in charge of u[%lld:%lld]\n", rank, start, end - 1);
        if (i != rank) continue;
        if (comRanks[0] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld:%lld] -> proc%d\n",
                    rank, start + comRanges[0] - ni, start + comRanges[1] - ni - 1, comRanks[0]);
        if (comRanks[1] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld:%lld] -> proc%d\n",
                    rank, start + comRanges[1] - ni, end - ni - 1, comRanks[1]);
        if (comRanks[2] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld] -> proc%d\n",
                    rank, start - 1, comRanks[2]);
        if (comRanks[3] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld] -> proc%d\n",
                    rank, end, comRanks[3]);
        if (comRanks[4] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld:%lld] -> proc%d\n",
                    rank, start + ni, start + ni + comRanges[2] - 1, comRanks[4]);
        if (comRanks[5] >= 0)
            fprintf(stderr, "[Proc%d data]: u[%lld:%lld] -> proc%d\n",
                    rank, start + ni + comRanges[2], start + ni + comRanges[3] - 1, comRanks[5]);
    }
#endif

    // alokacija memorije
    for (int i = 0; i < 6; ++i) {
        *(mem[i]) = (double*)malloc(length * sizeof(double));
    }

#ifdef _DEBUG
    for (int i = 0; i < length; ++i) {
        d[i] = start + i;
    }

    matrixMult(d, ad, w1, w2, length, start, ni, comRanks, comRanges);

    for (int i = 0; i < procNum; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (i != rank) continue;
        for (int j = 0; j < length; ++j) {
            fprintf(stderr, "Ad[%3lld] = %lf\n", start + j, ad[j]);
        }
    }
#endif

    // generiranje pocetnih vrijednosti algoritma
    generateRightSide(d, length, ni, nj, start, F, G);
    linearComb(0.0, r, -1.0, d, length);
    linearComb(0.0, x, 0.0, x, length);
    // (r[0] | r[0])
    oldRR = dotProd(r, r, length);
    MPI_Allreduce(MPI_IN_PLACE, &oldRR, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

#ifdef _DEBUG
    for (int i = 0; i < procNum; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (i != rank) continue;
        for (int j = 0; j < length; ++j) {
            fprintf(stderr, "b[%3lld] = %lf\n", start + j, d[j]);
        }
    }
#endif

    // algoritam
    while (oldRR > eps) {
#ifdef _DEBUG
        if (rank == 0) {
            fprintf(stderr, "%lf\n", oldRR);
        }
#endif
        // A * d[k]
        matrixMult(d, ad, w1, w2, length, start, ni, comRanks, comRanges);

        // (d[k] | A * d[k])
        dAd = dotProd(d, ad, length);
        MPI_Allreduce(MPI_IN_PLACE, &dAd, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        // tau[k]
        tau = oldRR / dAd;

        // r[k+1]
        linearComb(1.0, r, tau, ad, length);

        // zapocni (r[k+1] | r[k+1])
        newRR = dotProd(r, r, length);
        MPI_Iallreduce(MPI_IN_PLACE, &newRR, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD,
                       &reduceRequest);

        // x[k+1]
        linearComb(1.0, x, tau, d, length);

        //zavrsi (r[k+1] | r[k+1])
        MPI_Wait(&reduceRequest, MPI_STATUS_IGNORE);

        // d[k+1]
        linearComb(newRR / oldRR, d, -1.0, r, length);

        oldRR = newRR;
    }

#ifdef _DEBUG
    for (int i = 0; i < procNum; ++i) {
        MPI_Barrier(MPI_COMM_WORLD);
        if (i != rank) continue;
        for (int j = 0; j < length; ++j) {
            fprintf(stderr, "x[%3lld] = % 17.10le\n", start + j, x[j]);
        }
    }
#endif

    // ispis u datoteku
    sprintf(buff, "% 17.10le\n", 0.0);
    outSize = strlen(buff);

    MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL,
                  &fout);

    MPI_File_get_type_extent(fout, MPI_CHAR, &charExtent);
    MPI_File_set_size(fout, outSize * ni * nj * charExtent);

    MPI_File_set_view(fout, outSize * start * charExtent, MPI_CHAR, MPI_CHAR, "external32",
                      MPI_INFO_NULL);

    for (int i = 0; i < length; ++i) {
        sprintf(buff, "% 17.10le\n", x[i]);
        MPI_File_write(fout, buff, outSize, MPI_CHAR, MPI_STATUS_IGNORE);
    }

    MPI_File_close(&fout);

    for (int i = 0; i < 6; ++i) {
        free(*(mem[i]));
    }

    MPI_Finalize();
    return 0;
}
