#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <thrust/for_each.h>
#include <thrust/reduce.h>

typedef thrust::tuple<int, int, int, char> edge;

struct update {
    int last;

    update(int last) : last(last) {}

    template <typename Touple>
    __host__ __device__
    void operator()(Touple t) const {
        if (thrust::get<0>(t) == last) {
            thrust::get<3>(t) |= 1;
        }
        if (thrust::get<1>(t) == last) {
            thrust::get<3>(t) |= 2;
        }
    }
};

struct get_best {
    __host__ __device__
    edge operator()(const edge &a, const edge &b) const {
        int ca = thrust::get<3>(a), cb = thrust::get<3>(b);
        if ((ca & 1) + (ca >> 1) != 1) {
            return b;
        }
        if ((cb & 1) + (cb >> 1) != 1) {
            return a;
        }
        if (thrust::get<2>(a) > thrust::get<2>(b)) {
            return b;
        }
        return a;
    }
};

int main(int argc, char *argv[]) {
    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " root graph.dat\n";
        return -1;
    }
    int last = std::atoi(argv[1]);
    int n, m;
    std::ifstream fin(argv[2]);
    fin >> n >> m;
    thrust::host_vector<int> h_u(m), h_v(m), h_w(m);
    for (int i = 0; i < m; ++i) {
        fin >> h_u[i] >> h_v[i] >> h_w[i];
    }
    fin.close();

    thrust::device_vector<int> u(h_u), v(h_v), w(h_w);
    thrust::device_vector<char> c(m, 0);

    edge invalid(0, 0, 0, 0), ret;
    int cu, cv, cw;
    char code;

    for (int i = 1; i < n; ++i) {
        thrust::for_each(
            thrust::make_zip_iterator(make_tuple(
                u.begin(), v.begin(), w.begin(), c.begin())),
            thrust::make_zip_iterator(make_tuple(
                u.end(), v.end(), w.end(), c.end())),
        update(last));

        ret = thrust::reduce(
            thrust::make_zip_iterator(make_tuple(
                u.begin(), v.begin(), w.begin(), c.begin())),
            thrust::make_zip_iterator(make_tuple(
                u.end(), v.end(), w.end(), c.end())),
            invalid,
            get_best());

        code = thrust::get<3>(ret);
        cu = thrust::get<0>(ret);
        cv = thrust::get<1>(ret);
        cw = thrust::get<2>(ret);
        last = (code & 1) * cv + (code >> 1) * cu;

        std::cout << cu << ' ' << cv << ' ' << cw << '\n';
    }
    return 0;
}

